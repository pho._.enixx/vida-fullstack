import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
  <app-on-changes 
    *ngIf="destruir"
    title='Título alterado pelo App Component'
    [valorInicial]='valor'/>
  <router-outlet></router-outlet>
  <button (click)="destruirComponente()">Destruir</button>
  `
})
export class AppComponent implements OnInit {

  public valor: number = 30;
  public destruir: boolean = true;

  constructor() { }

  ngOnInit(): void {
    /**
     * Irá exibir algo no console ao startar o componente após 5s
     */
    setTimeout(() => {
      console.log(1);
    }, 5000)
  }

  mudarValor(){
    this.valor++;
  }

  destruirComponente() {
    this.destruir = false;
  }

}
