import { Component, Input, OnChanges, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-on-changes',
  templateUrl: './on-changes.component.html',
  styleUrls: ['./on-changes.component.scss']
})
export class OnChangesComponent implements OnChanges, OnDestroy {

  @Input() public title: string = 'Título do componente OnChanges';
  @Input() public valorInicial: number = 20;

  constructor() {}

  ngOnChanges(): void {
      console.log('O valor da variável Title foi alterado');
  }

  ngOnDestroy(): void {
    console.log("Um componente foi destruido!");
}

}
